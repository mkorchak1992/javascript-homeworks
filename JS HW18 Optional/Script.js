"use strict";

const user = {
    name: 'Dan',
    'last name': 'Broun',
    marks: {
        Informatics: 5,
        Math: 5,
        Geometry: 4,
        Olympiad: {
            Chemistry: 4,
            Biology: 5,
            Win: {
                Biology: 5,
            }
        }
    },
    characteristics: {
        height: 190,
        weight: 100,
        hair: 'brown'
    },
    arr:[1,3,5,7]
};

const cloneUser = deepClone(user);

function deepClone(obj) {
    let clonObj = {};
    for (let key in obj) {
        if (obj[key] instanceof Object && !(obj[key] instanceof Array)) {
            clonObj[key] = deepClone(obj[key]);
            continue;
        }
        clonObj[key] = obj[key];
    }
    return clonObj;
}

cloneUser.addNewProperty = {degree: 'Master'};
console.log(user);
console.log(cloneUser);