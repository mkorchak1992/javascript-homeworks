"use strict";

$(document).ready(function () {
    $('#jqNavLinks').on('click', 'a', function (event) {
        event.preventDefault();
        let id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});


function backToTop() {
    let $myArrowBtn = $('#myArrowBtn');
    $(window).on('scroll', () => {
        if ($(window).scrollTop() >= 50) {
            $myArrowBtn.fadeIn();
        } else {
            $myArrowBtn.fadeOut();
        }
    });
    $myArrowBtn.on('click', (e) => {
        e.preventDefault();
        $('body,html').animate({scrollTop: 0}, 2000);
    })
}
backToTop();

let $TopRatedBtn = $('.top-rated-btn');
$TopRatedBtn.on('click', () => {
    $('#topRatedSection').slideToggle('slow');
});