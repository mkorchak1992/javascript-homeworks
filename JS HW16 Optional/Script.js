"use strict";

//Loop

function plusCountFibonacci(n) {
    let numb1 = 0, numb2 = 1, sumNumb = 1;
    for (let i = 1; i <= n; i++) {
        sumNumb = numb1 + numb2;
        numb1 = numb2;
        numb2 = sumNumb;
    }
    return sumNumb;
}

function minCountFibonacci(n) {
    let numb1 = 0, numb2 = -1, sumNumb = -1;
    for (let i = -1; i >= n; i--) {
        sumNumb = numb1 + numb2;
        numb1 = numb2;
        numb2 = sumNumb;
    }
    return sumNumb;
}

let n = prompt('Enter the number', '0');
while (n === '' || isNaN(n)) {
    if (n === null) {
        break;
    }
    n = prompt('Enter the number');
}
n = parseInt(n);

if (n >= 0) {
    console.log(`Value of Fibonacci number is - (${plusCountFibonacci(n)})`);
}

if (n < 0) {
    console.log(`Value of Fibonacci number is - (${minCountFibonacci(n)})`);
}

//Рекурсия for +numbers

// function plusCountFibonacci(n) {
//     let result;
//     if (n === 0 || n === 1) {
//         return 1;
//     } else if (n>1){
//         result = plusCountFibonacci(n - 1) + plusCountFibonacci(n - 2);
//         return result;
//     }
// }


