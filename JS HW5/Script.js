"use strict";

function createNewUser() {

    this.firstName = prompt('Enter your First name: ', '');
    while (this.firstName === '' || !this.firstName) {
        if (this.firstName === null) {
            break;
        }
        this.firstName = prompt('Enter your First name AGAIN: ', '');
    }

    this.lastName = prompt('Enter you Last name', '');
    while (this.lastName === '' || !this.lastName) {
        if (this.lastName === null) {
            break;
        }
        this.lastName = prompt('Enter you Last name AGAIN: ', '');
    }

    this.birthday = prompt('Please enter your date of birth \nDay.Month.Year - with points between values', 'dd.mm.yyyy');
    while (!this.birthday) {
        this.birthday = prompt('Please enter your date of birth \nDay.Month.Year - with points between values', 'dd.mm.yyyy');
    }

// Метод getAge(),возвращает сколько пользователю лет.
    this.getAge = function () {
        let userBirthday = this.birthday.split('.');
        let now = new Date();
        let age = now.getFullYear() - +userBirthday[2];
        userBirthday = new Date(+userBirthday[2], +userBirthday[1] - 1, +userBirthday[0]);
        if (now < new Date(userBirthday.setFullYear(now.getFullYear()))) {
            age = +age - 1;
        }
        return (`${this.firstName} you are ${age} years old`);
    };

//Метод getLogin(),возвращает первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре.
    this.getLogin = function () {
        let login = (this.firstName[0] + this.lastName).toLowerCase();
        return (`Your login is: ${login}`);
    };

//Метод getPassword(),возвращает первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения.
    this.getPassword = function () {
        let password = (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6));
        return (`Your password is: ${password}`);
    };

    this.setFirstName = function (newFirstName) {
        delete this.firstName;
        this.firstName = newFirstName;
    };

    this.setLastName = function (newLastName) {
        delete this.lastName;
        this.lastName = newLastName;
    };
}

let newUser = new createNewUser();

Object.defineProperties(newUser, {
    "firstName": {
        writable: false,
    },
    "lastName": {
        writable: false,
    },
});

// Try to change firstName and lastName
// newUser.setFirstName('Ben');
// newUser.setLastName('Affleck');

console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());



