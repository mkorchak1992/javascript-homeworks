"use strict";

let someUkrCities = ['Cities', ['Kyiv', 'Vinnytsia', 'Odessa', 'Lviv', 'Ivano-Frankivsk', 'Kharkiv'], {
    Year: 2020, Country: 'Ukraine'
},];

function showListElem(list) {
    const div = document.createElement('div');
    document.body.prepend(div);
    div.className = 'pop-up-list';

    function addList(div, arr) {
        let ul = document.createElement('ul');
        div.append(ul);
        let li;

        arr.map((item) => {
            if (Array.isArray(item)) {
                addList(li, item);
                return;
            }
            if (item.constructor === Object) {
                addObjElem(li, item);
                return;
            }
            li = document.createElement('li');
            li.append(item);
            ul.append(li);

        });

        function addObjElem(div, arr) {
            for (let key in arr) {
                let li = document.createElement('li');
                li.append(`${key} : ${arr[key]}`);
                ul.append(li);
            }
        }
    }

    addList(div, list);

    div.insertAdjacentHTML("afterend", `<div id="timer"><span class="display">10</span></div>`);
    let display = document.querySelector('#timer .display');
    let timeLeft = parseInt(display.innerHTML);

    let timeOut = setInterval(function () {
        if (--timeLeft >= 0) {
            display.innerHTML = `${timeLeft}`;
        } else {
            document.querySelector('#timer .display').style.display = 'none';
            document.querySelector('.pop-up-list').style.display = 'none';
            clearInterval(timeOut);
        }
    }, 1000);

}

showListElem(someUkrCities);

