"use strict";

let number = prompt('Please,write a Number', '0');
while (number === '' || isNaN(number)) {
    if (number === null) {
        break;
    }
    number = prompt('Please,write a Number', number);
}
number = parseInt(number);

function findFactorial(number) {
    let result;
    if (number < 0) {
        console.log('Wrong number');
    } else if (number === 1) {
        return 1;
    } else {
        result = findFactorial(number - 1) * number;
        return result;
    }

}

console.log(findFactorial(number));
