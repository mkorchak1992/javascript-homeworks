"use strict";
// Easy level

let userNumber = prompt('Please enter your number', '0');
while (userNumber % 1 !== 0) {
    userNumber = prompt('Please enter your number again without point and letters', '0');
}
for (let i = 0; i <= userNumber; i++) {
    if (userNumber < 5 || userNumber == null || userNumber === '') {
        alert('Sorry, no numbers');
        break;
    } else if (i % 5 === 0) {
        console.log(i);
    }
}

//More difficult level.(Optional)
//
// let userNumber1 = +prompt('Enter your FIRST number', '0');
// while (!Number.isInteger(userNumber1)) {
//     userNumber1 = +prompt('Please enter your FIRST number without points and letters', '0');
// }
//
// let userNumber2 = +prompt('Enter your SECOND number', '0');
// while (!Number.isInteger(userNumber2)) {
//     userNumber2 = +prompt('Please enter your SECOND number without points and letters', '0');
// }
//
// while (userNumber1 === userNumber2) {
//     alert('This 2 numbers must be DIFFERENT...');
//     userNumber1 = +prompt('Please enter your FIRST number without points and letters', '0');
//     userNumber2 = +prompt('Please enter your SECOND number without points and letters', '0');
// }
// let maxNumber = Math.max(userNumber1, userNumber2);
// let minNumber = Math.min(userNumber1, userNumber1);
//
// for (let i = minNumber; i<= maxNumber; i++) {
//     if(i % 2 !==0 && i !==2 && i!==1){
//         console.log(i);
//     }
// }
