"use strict";
let form = document.querySelector('.password-form');
let firstPassword = document.getElementById('password');
let secondPassword = document.getElementById('confirmPassword');


form.addEventListener('click', function (event) {
    if (event.target.dataset.icon !== undefined) {
        event.target.classList.toggle('fa-eye-slash');
        event.target.classList.toggle('fa-eye');
    }
});

form.addEventListener('click', function (event) {
    if (!event.target.dataset.password) {
        if (event.target.nextElementSibling.classList.contains('fa-eye-slash')) {
            event.target.removeAttribute('type');
            event.target.setAttribute('type', 'text');
        } else if (event.target.nextElementSibling.classList.contains('fa-eye')) {
            event.target.removeAttribute('type');
            event.target.setAttribute('type', 'password');
        }

    }
});

form.addEventListener("submit", e => {
    e.preventDefault();
    if (firstPassword.value === secondPassword.value && firstPassword.value !== '' && secondPassword.value !== '') {
        errorMessage.innerText = '';
        setTimeout (()=>{
            alert('You are welcome');
        },100);
    } else {
        errorMessage.innerText = 'Нужно ввести одинаковые значения';
    }
});
