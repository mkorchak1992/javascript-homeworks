'use strict';

let countWorkingTime = () => {
    let speedOfDevelopers = [15, 20, 10, 15];
    let pointsOfProjectTasks = [50, 10, 40, 10, 5, 50, 20];
    let deadline = new Date(2020, 6, 1);
    deadline = new Date(deadline.getFullYear(), deadline.getMonth() - 1, deadline.getDate());

    function arraySum(array) {
        let sum = 0;
        for (let i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    let oneDayDevWork = arraySum(speedOfDevelopers);
    let allProjectPoints = arraySum(pointsOfProjectTasks);
    let allProjectHours = (allProjectPoints * 8) / oneDayDevWork;
    let allProjectDays = allProjectHours / 8;

    let countDays = () => {
        let now = new Date();
        let days = 0;
        while (now.getFullYear() !== deadline.getFullYear() || now.getMonth() !== deadline.getMonth() || now.getDate() !== deadline.getDate()) {
            if (deadline < now) {
                console.log('Wrong deadlineTime...');
                break;
            } else if (now.getDay() !== 0 && now.getDay() !== 6) {
                days++;
            }
            now.setDate(now.getDate() + 1);
        }
        return days;
    };

    let availableDays = countDays();

    if (availableDays > allProjectDays) {
        return (`Все задачи будут успешно выполнены за ${(availableDays - allProjectDays).toFixed(1)} дней до наступления дедлайна!`);
    } else if (availableDays < allProjectDays) {
        return (`Команде разработчиков придется потратить дополнительно ${((allProjectDays - availableDays) * 8).toFixed(1)} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
    }
};

console.log(countWorkingTime());

