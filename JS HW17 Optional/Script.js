"use strict";

const student = {
 tabel:{}
};

//Cпросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
let userName = prompt('Enter your Name');
let userLastName = prompt('Enter your Last Name');
while (!userName || !userLastName || +userName || +userLastName) {
    userName = prompt('Enter your Name again', userName);
    userLastName = prompt('Enter your Last Name again', userLastName);
    if (userName === null || userLastName === null) {
        break;
    }
}
student.name = userName;
student["last name"] = userLastName;

//В цикле спрашивать у пользователя название предмета и оценку по нему.
//Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
let userSubject = prompt('Please enter the subject');
let subjectMark =+prompt(`Please enter the Mark for - ${userSubject}`);
while (true) {
    student.tabel[userSubject] = subjectMark;
    userSubject = prompt('Please enter the subject');
    subjectMark =+prompt(`Please enter the Mark for - ${userSubject}`);
    if (userSubject === null || subjectMark === null) {
        break;
    }
}

//Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
let badMarks = false;
for (let key in student.tabel) {
    if (student.tabel[key] < 4) {
        badMarks = true;
        break;
    }
}
if (!badMarks) {
    console.log(`Студент ${student.name} ${student['last name']} переведен на следующий курс.`);
}

//Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.
let userSubjectCount = 0;
let averageMark = 0;
for (let key in student.tabel) {
    userSubjectCount++;
    averageMark += student.tabel[key];
}
student.tabel.averageMark = averageMark / userSubjectCount;

if (student.tabel.averageMark > 7) {
    console.log(`Студенту - ${student.name} ${student['last name']} назначена степендия.`);
}

console.log(student);