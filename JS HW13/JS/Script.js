"use strict";

const themeButton = document.getElementsByClassName('themeButton ')[0];
let cssLink = document.getElementsByTagName('link')[1];

window.onload = function () {
    if (localStorage.getItem('styleCss')) {
        cssLink.setAttribute('href', localStorage.getItem('styleCss'));
    }
};

themeButton.addEventListener('click', () => {
    if (cssLink.getAttribute('href') === 'css/style.css') {
        cssLink.setAttribute('href', "css/style2.css");
        localStorage.setItem('styleCss', 'css/style2.css');
    } else if (cssLink.getAttribute('href') === 'css/style2.css') {
        cssLink.setAttribute('href', "css/style.css");
        localStorage.setItem('styleCss', 'css/style.css');
    }
});