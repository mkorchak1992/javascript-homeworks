"use strict";

//(1й варіант)

const newUser = {
    getLogin: function () {
        return newUser.login = (this.firstName[0] + this.lastName).toLowerCase();
    },
    set setFirstName(newFirstName) {
        delete this.firstName;
        this.firstName = newFirstName;
    },
    set setLastName(newLastName) {
        delete this.lastName;
        this.lastName = newLastName;
    }
};

let firstName;
let lastName;

function createNewUser(firstName, lastName) {
    while (!firstName || !lastName) {
        firstName = prompt('Please enter your name', firstName);
        lastName = prompt('Please enter your lastName', lastName);
        if (firstName === null && lastName === null) {
            break;
        }
        newUser.firstName = firstName;
        newUser.lastName = lastName;
    }
    return newUser;
}

createNewUser();

Object.defineProperties(newUser, {
    "firstName": {
        writable: false,
    },
    "lastName": {
        writable: false,
    },
});

newUser.getLogin();
console.log(newUser);

//(КОНСТРУКТОР 2й варіант)

// function createNewUser() {
//     this.firstName = prompt('Enter your First name: ', '');
//     while (this.firstName === '' || !this.firstName) {
//         if (this.firstName === null) {
//             break;
//         }
//         this.firstName = prompt('Enter your First name AGAIN: ', '');
//     }
//     this.lastName = prompt('Enter you Last name', '');
//     while (this.lastName === '' || !this.lastName) {
//         if (this.lastName === null) {
//             break;
//         }
//         this.lastName = prompt('Enter you Last name AGAIN: ', '');
//     }
//
//     this.getLogin = function () {
//         let login = (this.firstName[0] + this.lastName).toLowerCase();
//         return login;
//     };
//
//     this.setFirstName = function (newFirstName) {
//         delete this.firstName;
//         this.firstName = newFirstName;
//     };
//
//     this.setLastName = function (newLastName) {
//         delete this.lastName;
//         this.lastName = newLastName;
//     };
// }
//
// let newUser = new createNewUser();
//
// Object.defineProperties(newUser, {
//     "firstName": {
//         writable: false,
//     },
//     "lastName": {
//         writable: false,
//     },
// });
//
// console.log(newUser);
// console.log(`Your login is: ${newUser.getLogin()}`);

//Try to change firstName and lastName
// newUser.setFirstName('Ben');
// newUser.setLastName('Affleck');
// console.log(newUser);

