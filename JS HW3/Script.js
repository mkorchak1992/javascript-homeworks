"use strict";
let number1 = prompt('Enter 1-st number', '0');
while (isNaN(number1) || number1 === "" || number1 === null) {
    number1 = prompt('Enter again 1-st number', number1);
}
let number2 = prompt('Enter 2-nd number', '0');
while (isNaN(number2) || number2 === "" || number2 === null) {
    number2 = prompt('Enter again number', number2);
}
let operation = prompt('Enter what you wish to do with numbers,\n( + )\n( * )\n( - )\n( / )', '');
while (operation !== '+' && operation !== '*' && operation !== '-' && operation !== '/') {
    operation = prompt('Try again \n( + )\n( * )\n( - )\n( / )', '');
}

function calculator(number1, number2, operation) {
    if (operation === '+') {
        return +number1 + +number2;
    } else if (operation === '*') {
        return number1 * number2;
    } else if (operation === '-') {
        return number1 - number2;
    } else if (operation === '/') {
        return number1 / number2;
    }
}

console.log(calculator(number1, number2, operation));