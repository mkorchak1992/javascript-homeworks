"use strict";

let userName;
let userAge;
do {
    userAge = prompt('Please enter your Age',);
}
while ((isNaN(userAge)) || (userAge === null) || (userAge === ""));

do {
    userName = prompt('Please enter your Name?', '');
}
while (!userName);

function checkAge(userAge) {
    if (userAge < 18) {
        return false;
    } else if (userAge >= 18 && userAge <= 22) {
        return confirm('Are you sure you want to continue?');
    } else {
        return true;
    }
}

if (checkAge(userAge)) {
    alert('Welcome ' + userName);
} else {
    alert('You are not allowed to visit this website.');
}

checkAge();
