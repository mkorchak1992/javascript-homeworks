"use strict";

function filterBy(dataList, verificationData) {
    let newDataList = dataList.filter(element => typeof (element) !== typeof (verificationData));
    return newDataList;
}

console.log(filterBy(['Hello',8,'Ukraine', {name:"Alonso"},[], '27', null,100,undefined], "Hi"));


//null is object in JS !
// function filterBy(dataList, verificationData) {
//     let newDataList = [];
//
//     if (verificationData === null) {
//         newDataList=dataList.filter(element => element !==null);
//     } else {
//         newDataList = dataList.filter(element => typeof (element) !== typeof (verificationData));
//     }
//     return newDataList;
// }
